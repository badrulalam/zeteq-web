(function(module) {
    
    module.config(function ($stateProvider) {
        $stateProvider.state('join_us', {
            url: '/join_us',
            views: {
                "main": {
                    controller: 'Join_usController as model',
                    templateUrl: 'join_us/join_us.tpl.html'
                }
            },
            data:{ pageTitle: 'Join Us' }
        });
    });
    
}(angular.module('zeteqApp.joinUs', [
    'ui.router'
])));
