(function(app) {

    app.config(function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/');
    });

    app.run(function () {});

    app.controller('AppController', function ($scope) {

        angular.element(document).ready(function () {

            $(window).scroll(function () {
                if ($(this).scrollTop() > 600) {
                    $('#scroll_top').fadeIn();
                } else {
                    $('#scroll_top').fadeOut();
                }
            });
            $("a[href='#top']").click(function () {
                $("html, body").animate({scrollTop: 0}, "slow");
                return false;
            });

        });

        $(window).load(function () {
            // Animate loader off screen
            $(".se-pre-con").fadeOut("slow");
        });

    });

}(angular.module("zeteqApp", [
    'zeteqApp.home',
    'templates-app',
    'templates-common',
    'ui.router.state',
    'ui.router',
    'zeteqApp.portfolio',
    'zeteqApp.clients',
    'zeteqApp.contact',
    'zeteqApp.joinUs',
    'zeteqApp.blog',
    'zeteqApp.aboutUs',
])));
