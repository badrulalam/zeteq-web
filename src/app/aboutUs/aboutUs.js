(function(module) {
    
    module.controller('AboutUsController', function () {
        var model = this;

        init();

        function init() {

        }

        angular.element(document).ready(function () {

            $("#owl-demo").owlCarousel({

                autoPlay: 6000, //Set AutoPlay to 3 seconds

                //navigation : true, // Show next and prev buttons
                //stopOnHover : true,
                paginationSpeed : 1000,
                goToFirstSpeed : 2000,
                slideSpeed : 300,
                paginationSpeed : 400,
                singleItem:true,
                //transitionStyle:"fade"

                // "singleItem:true" is a shortcut for:
                // items : 1,
                // itemsDesktop : false,
                // itemsDesktopSmall : false,
                // itemsTablet: false,
                // itemsMobile : false

            });

        });
    });
    
}(angular.module("zeteqApp.aboutUs")));