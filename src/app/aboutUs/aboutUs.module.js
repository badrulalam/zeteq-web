(function(module) {
    
    module.config(function ($stateProvider) {
        $stateProvider.state('aboutUs', {
            url: '/aboutUs',
            views: {
                "main": {
                    controller: 'AboutUsController as model',
                    templateUrl: 'aboutUs/aboutUs.tpl.html'
                }
            },
            data:{ pageTitle: 'AboutUs' }
        });
    });
    
}(angular.module('zeteqApp.aboutUs', [
    'ui.router'
])));
