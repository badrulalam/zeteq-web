(function(module) {
    
    module.config(function ($stateProvider) {
        $stateProvider.state('portfolio', {
            url: '/portfolio',
            views: {
                "main": {
                    controller: 'PortfolioController as model',
                    templateUrl: 'portfolio/portfolio.tpl.html'
                }
            },
            data:{ pageTitle: 'Portfolio' }
        });
    });
    
}(angular.module('zeteqApp.portfolio', [
    'ui.router'
])));
