(function(module) {
    
    module.config(function ($stateProvider) {
        $stateProvider.state('clients', {
            url: '/clients',
            views: {
                "main": {
                    controller: 'ClientsController as model',
                    templateUrl: 'clients/clients.tpl.html'
                }
            },
            data:{ pageTitle: 'Clients' }
        });
    });
    
}(angular.module('zeteqApp.clients', [
    'ui.router'
])));
