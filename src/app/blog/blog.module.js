(function(module) {
    
    module.config(function ($stateProvider) {
        $stateProvider.state('blog', {
            url: '/blog',
            views: {
                "main": {
                    controller: 'BlogController as model',
                    templateUrl: 'blog/blog.tpl.html'
                }
            },
            data:{ pageTitle: 'Blog' }
        });
    });
    
}(angular.module('zeteqApp.blog', [
    'ui.router'
])));
