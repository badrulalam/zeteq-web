angular.module('templates-app', ['aboutUs/aboutUs.tpl.html', 'blog/blog.tpl.html', 'clients/clients.tpl.html', 'contact/contact.tpl.html', 'home/home.tpl.html', 'join_us/join_us.tpl.html', 'portfolio/portfolio.tpl.html']);

angular.module("aboutUs/aboutUs.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("aboutUs/aboutUs.tpl.html",
    "<div class=\"about-us-wrapper\">\n" +
    "\n" +
    "    <div id=\"owl-demo\" class=\"owl-carousel owl-theme\">\n" +
    "\n" +
    "        <div class=\"item\">\n" +
    "            <div class=\"about-us-item-text\">\n" +
    "                <h5>Get to know us</h5>\n" +
    "                <h1>About us</h1>\n" +
    "                <h4>We are a small but highly efficient and coherent group\n" +
    "                    <br/>if designers/programmers/analysts. We spend our\n" +
    "                    <br/>nights coding so you done have to\n" +
    "                </h4>\n" +
    "            </div>\n" +
    "            <img class=\"img-responsive\" src=\"assets/images/about-us.png\" alt=\"\"/>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "\n" +
    "</div>");
}]);

angular.module("blog/blog.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("blog/blog.tpl.html",
    "<h1>Blog</h1>\n" +
    "");
}]);

angular.module("clients/clients.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("clients/clients.tpl.html",
    "<div id=\"clients-wrapper\">\n" +
    "    <div class=\"container-fluid\">\n" +
    "\n" +
    "        <h1>Clients</h1>\n" +
    "\n" +
    "        <div class=\"col-md-12\">\n" +
    "\n" +
    "            <div class=\"col-md-2\">\n" +
    "                <div class=\"client-box-wrapper\">\n" +
    "                    <h3>EWG</h3>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"col-md-2\">\n" +
    "                <div class=\"client-box-wrapper\">\n" +
    "                    <h3>Asia Foundation</h3>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"col-md-2\">\n" +
    "                <div class=\"client-box-wrapper\">\n" +
    "                    <h3>OTIS</h3>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"col-md-2\">\n" +
    "                <div class=\"client-box-wrapper\">\n" +
    "                    <h3>Lakehead</h3>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"col-md-2\">\n" +
    "                <div class=\"client-box-wrapper\">\n" +
    "                    <h3>Life Group</h3>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"col-md-2\">\n" +
    "                <div class=\"client-box-wrapper\">\n" +
    "                    <h3>Bangla Trac</h3>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"col-md-2\">\n" +
    "                <div class=\"client-box-wrapper\">\n" +
    "                    <h3>Haroon</h3>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"col-md-2\">\n" +
    "                <div class=\"client-box-wrapper\">\n" +
    "                    <h3>NASSA</h3>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"col-md-2\">\n" +
    "                <div class=\"client-box-wrapper\">\n" +
    "                    <h3>Goynaa</h3>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"col-md-2\">\n" +
    "                <div class=\"client-box-wrapper\">\n" +
    "                    <h3>Odyssey</h3>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"col-md-2\">\n" +
    "                <div class=\"client-box-wrapper\">\n" +
    "                    <h3>Akabe</h3>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"col-md-2\">\n" +
    "                <div class=\"client-box-wrapper\">\n" +
    "                    <h3>BTCL</h3>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("contact/contact.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("contact/contact.tpl.html",
    "<div class=\"contact-us-wrapper\">\n" +
    "    <iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d1290.782995354238!2d90.41465515744409!3d23.787086475807047!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1444113848639\" width=\"600\" height=\"550\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>\n" +
    "\n" +
    "    <div class=\"col-md-12\">\n" +
    "\n" +
    "        <div class=\"col-md-6\">\n" +
    "            <h1>Contact Us</h1>\n" +
    "            <h3>Zeteq Systems</h3>\n" +
    "            <p>House 6, Road 33, Gulshan 1, Dhaka-1212</p>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"col-md-6\">\n" +
    "\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>\n" +
    "\n" +
    "\n" +
    "");
}]);

angular.module("home/home.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("home/home.tpl.html",
    "<div id=\"owl-demo\" class=\"owl-carousel owl-theme home-wrapper\">\n" +
    "\n" +
    "    <div class=\"item home-wrapper-item\">\n" +
    "        <div class=\"container\">\n" +
    "            <div class=\"home-wrapper-item-text\">\n" +
    "                <h5>Welcome to zeteq</h5>\n" +
    "                <h1>The Coding Ninjas</h1>\n" +
    "                <h6>Your friendly neightbourhood coding ninjas...</h6>\n" +
    "            </div>\n" +
    "            <div class=\"row home-wrapper-item-img\">\n" +
    "                <div class=\"col-sm-3\">\n" +
    "                    <img class=\"img-responsive\" src=\"assets/images/slide1/codingNinjas-1.png\" alt=\"\"/>\n" +
    "                </div>\n" +
    "\n" +
    "                <div class=\"col-sm-3\">\n" +
    "                    <img src=\"assets/images/slide1/codingNinjas-2.png\" alt=\"\"/>\n" +
    "                </div>\n" +
    "\n" +
    "                <div class=\"col-sm-3\">\n" +
    "                    <img src=\"assets/images/slide1/codingNinjas-3.png\" alt=\"\"/>\n" +
    "                </div>\n" +
    "\n" +
    "                <div class=\"col-sm-3\">\n" +
    "                    <img src=\"assets/images/slide1/codingNinjas-4.png\" alt=\"\"/>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"item home-wrapper-item\">\n" +
    "        <div class=\"container\">\n" +
    "\n" +
    "            <div class=\"row\">\n" +
    "\n" +
    "                <div class=\"col-sm-9\">\n" +
    "                    <div class=\"home-wrapper-item-text home-wrapper-item-text-common\">\n" +
    "                        <h5>Welcome to The Future</h5>\n" +
    "                        <h1>Mean</h1>\n" +
    "                        <h6>We Develope cutting edge web applications <br/>on the Mongo/Express/Angular running on the<br/> Node.js web server.\n" +
    "                            <br/><br/>\n" +
    "                            If you are looking for ultra fast, desktop styled<br/>web applications, that can also handle big data<br/>\n" +
    "                            than mean is your friend.\n" +
    "                        </h6>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "\n" +
    "                <div class=\"col-sm-3\">\n" +
    "                    <div class=\"home-wrapper-item-img home-wrapper-item-img-common\">\n" +
    "                        <img class=\"img-responsive\" src=\"assets/images/slide2/mean.png\" alt=\"\"/>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"item home-wrapper-item\">\n" +
    "        <div class=\"container\">\n" +
    "\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-sm-7\">\n" +
    "                    <div class=\"home-wrapper-item-text home-wrapper-item-text-common\">\n" +
    "                        <h5>Mobile Apps for IOS ANDROID AND WINDOWS</h5>\n" +
    "                        <h1>Mobile</h1>\n" +
    "                        <h6>IF YOU ARE LOOKING FOR A MOBILE APPLICATION <br/>THAT CAN RUN ACCROSS PLATFORM AND THE DEVELOPMENT COST WON'T<br/>\n" +
    "                            BREAK YOUR POCKET THEN YOU CAN CONSIDER HYBRID MOBILE APPLICTION.\n" +
    "                            <br/><br/>\n" +
    "                            We develop mobile applications on IONIC, Apache cordova and the Phonegap<br/>platform.\n" +
    "                        </h6>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "\n" +
    "                <div class=\"col-sm-5\">\n" +
    "                    <div class=\"home-wrapper-item-img home-wrapper-item-img-common\">\n" +
    "                        <img class=\"img-responsive\" src=\"assets/images/slide3/mobile.png\" alt=\"\"/>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"item home-wrapper-item\">\n" +
    "        <div class=\"container\">\n" +
    "\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-sm-8\">\n" +
    "                    <div class=\"home-wrapper-item-text home-wrapper-item-text-common home-wrapper-item-text-graphics\">\n" +
    "                        <h5>We are brains with a sense of beauty</h5>\n" +
    "                        <h1>Graphics</h1>\n" +
    "                        <h6>No matter how great your app is, how many features <br/>it got, if it does not look good\n" +
    "                            it probably is<br/> not going to make it big. Not only do we develop<br/> your product, but we\n" +
    "                            provide the complete end<br/> to end graphics solutions, starting from the<br/> logo, user interface, branding etc.\n" +
    "                        </h6>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "\n" +
    "                <div class=\"col-sm-4\">\n" +
    "                    <div class=\"home-wrapper-item-img home-wrapper-item-img-common\">\n" +
    "                        <img class=\"img-responsive\" src=\"assets/images/slide4/graphics.png\" alt=\"\"/>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "\n" +
    "    <div class=\"item home-wrapper-item\">\n" +
    "        <div class=\"container\">\n" +
    "\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-sm-7\">\n" +
    "                    <div class=\"home-wrapper-item-text home-wrapper-item-text-common\">\n" +
    "                        <h5>ROCK SOLID PLATFORM</h5>\n" +
    "                        <h1>Lamp</h1>\n" +
    "                        <h6>SCALABLE SECURE AND FAST WEB BASED APPLICATIONS<br/>AND WEBSITES RUNNING ON THE CLASSIC AND <br/>\n" +
    "                            PROVEN LINUX/APACHE/MYSQL AND PHP. <br/>\n" +
    "                            WE DEVELOP OUR SOLUTIONS ON THE RENOWNED<br/>SYMPHONY PLATFORM.\n" +
    "                        </h6>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "\n" +
    "                <div class=\"col-sm-5\">\n" +
    "                    <div class=\"home-wrapper-item-img home-wrapper-item-img-common\">\n" +
    "                        <img class=\"img-responsive\" src=\"assets/images/slide5/lamp.png\" alt=\"\"/>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"item home-wrapper-item\">\n" +
    "        <div class=\"container\">\n" +
    "\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-sm-7\">\n" +
    "                    <div class=\"home-wrapper-item-text home-wrapper-item-text-common\">\n" +
    "                        <h5>Communication Applications on</h5>\n" +
    "                        <h1>Xmpp</h1>\n" +
    "                        <h6>We develop online communication and collaboration<br/>tools on the renowned Ejabberd server. If you are\n" +
    "                            <br/>looking to develop an application that requires <br/>real time interaction between the users, then XMPP\n" +
    "                            <br/>is the way to go...\n" +
    "                        </h6>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "\n" +
    "                <div class=\"col-sm-5\">\n" +
    "                    <div class=\"home-wrapper-item-img home-wrapper-item-img-common\">\n" +
    "                        <img class=\"img-responsive\" src=\"assets/images/slide6/xmpp.png\" alt=\"\"/>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"item home-wrapper-item\">\n" +
    "        <div class=\"container\">\n" +
    "\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-sm-7\">\n" +
    "                    <div class=\"home-wrapper-item-text home-wrapper-item-text-common\">\n" +
    "                        <h5>Geograpic and mapping applications</h5>\n" +
    "                        <h1>Gis</h1>\n" +
    "                        <h6>We develop applications for analyzing<br/>and working with geospatial data. We use geoserver\n" +
    "                            <br/>along with POSTGIS for\n" +
    "                        </h6>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "\n" +
    "                <div class=\"col-sm-5\">\n" +
    "                    <div class=\"home-wrapper-item-img home-wrapper-item-img-common\">\n" +
    "                        <img class=\"img-responsive\" src=\"assets/images/slide7/gis.png\" alt=\"\"/>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "</div>");
}]);

angular.module("join_us/join_us.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("join_us/join_us.tpl.html",
    "<h1>Join us</h1>\n" +
    "");
}]);

angular.module("portfolio/portfolio.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("portfolio/portfolio.tpl.html",
    "<h1>Portfolio</h1>\n" +
    "");
}]);
